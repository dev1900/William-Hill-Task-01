# Simple k8s setup with basic pipeline

The purpose of this setup is to create simple k8s config based on given requirements in task `01`

### Prerequisites

- Up and running k8s cluster in DO
- DO token saved in repo secrets as `DO_TOKEN`
- DO cluster name saved in secrets as `CLUSTER_NAME`

### Content

- namespace men01 where setup will be applied
- deployment with nginx container by default with 2 pods
- NodePort service
- basic pdb and hpa
### Flow

- run `npm run apply`
or
- changes will be automatically applied to DO on push to master

## About
- standard k8s manifest, next step will be create helm chart with values
- script is basic only without tests. Next step will be choose ci/cd system to deploy it with tests.
- missing monitoring but with health probes set to /. Better will be set it to real health path
- need to tune probes and hpa byt performance testing
- missing ingress and a lot other cool stuff

